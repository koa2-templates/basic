FROM node:8
LABEL maintainer dennis.ge<gedennis@163.com>

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY package.json /usr/src/app/
RUN npm config set registry https://registry.npm.taobao.org && npm i --production --verbose

COPY . /usr/src/app

CMD [ "node", "app.js" ]

# replace this with your application's default port
EXPOSE 1226
