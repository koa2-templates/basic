# Koa2-scaffold  
Koa2-scaffold is a template for koa2 app with common components, such as logger,
error handler, test ,CI and so on.

## 基础特性

* 多环境配置: [node-config](https://github.com/lorenwest/node-config)
* 路由管理: [koa-router](https://github.com/alexmingoia/koa-router)
* 日志管理: [winston](https://github.com/winstonjs/winston)
* 代码风格：[Eslint](https://eslint.org/)
* 测试框架： [ava](https://github.com/avajs/ava) + [nyc](https://github.com/istanbuljs/nyc)
* 持续集成： gitlab ci
* 容器化：docker
* 错误管理： 错误统一管理中间件和错误管理平台 [sentry](https://sentry.io/welcome/)

## 运行

最好将 npm registry 设置成淘宝，建议安装使用[nrm](https://github.com/Pana/nrm)。

```
$ nrm use taobao
```


## 测试

建议大家写单元测试。测试框架使用 ava，覆盖率采用 nyc。

```
// 只运行测试
$ npm run test

// 测试及代码覆盖率
$ npm run test:cov
```

## 团队规范
### 命名规范
- 变量命名： 采用小驼峰命名。
```
const userInfo = { name: 'cma' };
```
- 常量命名： 全大写，单词通过下划线连接，尽量语义明确但精简。
```
const NOT_ALLOW_RCMD = { ... };
```
- 方法命名： 采用小驼峰命名, 但多为动词。
```
const getUserInfo = { ... };
```
- 类命名： 采用大驼峰命名。
```
const UserSchema = { ... };
```
- 文件命名： 采用snake命名,单词通过下划线连接。
```
status_code.js
```

### 分支管理

建议大家熟悉 git 工作流。可以[这篇文章](https://danielkummer.github.io/git-flow-cheatsheet/index.zh_CN.html)及类似的文章。

* master 分支： 用于最后发版本。
* develop 分支：用于平时发测试的集成版本。
* feature分支： 如 feature/user 分支，用于各个特性的开发。

这是最基本的开发流程。每个特性分支开发完成之后，在 gitlab 上开 merge request。由专门人员 review 之后，merge 到 develop 分支。

### commit 管理

大家需要规范 commit log，项目使用 angular 的[cz-conventional-changelog](https://github.com/commitizen/cz-conventional-changelog)约定。

```
 feat:     A new feature
 fix:      A bug fix
 chore:    
 docs:     Documentation only changes
 style:    Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
 refactor: A code change that neither fixes a bug nor adds a feature
 perf:     A code change that improves performance
 test:     Adding missing tests or correcting existing tests
```

建议大家本地安装[commitizen](https://github.com/commitizen/cz-cli)。commit 的时候输入系列命令，选择输入即可。

```
$ git cz
```

### Eslint

代码需要遵循 elsint 规范，目前使用Airbnb base.

## More
Get started ！ Enjoy !!!