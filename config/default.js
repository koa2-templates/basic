module.exports = {
  app: {
    name: 'koa2-scaffold',
    env: 'base',
  },

  http: {
    port: 1226,
  },

  logger: {
    console: {
      enable: true,
      transport: 'Console',
      option: {
        json: true,
        stringify: true,
        timestamp: true,
        logstash: true,
        humanReadableUnhandledException: true,
      },
    },
  },

  mongo: {
    testDB: {
      hosts: ['localhost:27017'],
      database: 'test',
      options: {
        db: {
          readPreference: 'secondaryPreferred',
        },
      },
    },
  },
  // mysql配置
  mysql: {
    name: 'test',
    username: 'test',
    password: 'teste',
    options: {
      host: 'localhost',
      port: '3306',
    },
    logging: false,
  },

  memcached: {
    hosts: ['1.1.1.1:11211'],
    options: {},
  },

  // jpush配置
  jpush: {
    appKey: '',
    masterSecret: '',
    isDebug: true,
  },

  sentry: {
    enable: false,
    dsn: '',
    options: {
      captureUnhandledRejections: true,
    },
  },

  wechat: {
    host: '',
    account: {
      token: '',
      appId: '',
      appSecret: '',
      encodingAESKey: '',
    },
    menu: {
      button: [
        {
          type: 'view',
          name: 'test1',
          state: 'test1',
        },
        {
          type: 'view',
          name: 'test2',
          state: 'test2',
        },
        {
          name: '更多',
          sub_button: [
            {
              type: 'view',
              name: 'test3',
              state: 'test3',
            },
            {
              type: 'view',
              name: 'test4',
              state: 'test4',
            },
          ],
        },
      ],
    },

    views: {},
    subscribeMessage: '',
    template: {
      t1: {
        id: '',
        url: '',
      },
    },
    jsConfig: {
      debug: false,
      jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'hideAllNonBaseMenuItem', 'showAllNonBaseMenuItem'],
    },
    replies: {
      1: 'hahah<a href="[testUrl]">笑嘻嘻嘻嘻>></a>',
      default: '呵呵呵',
    },
  },
};
