module.exports = {
  apps: [
    {
      name: 'mt_demo',
      script: 'app.js',
      instance_var: 'INSTANCE_ID',
      env: {
        NODE_ENV: 'dev',
        NODE_CONFIG_DIR: './config/',
      },
      env_test: {
        NODE_ENV: 'test',
        NODE_CONFIG_DIR: './config/',
      },
      env_production: {
        NODE_ENV: 'prod',
        NODE_CONFIG_DIR: './config/',
      },
    },
  ],
};
