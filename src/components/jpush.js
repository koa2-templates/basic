const JPush = require('jpush-sdk');
const Promise = require('bluebird');

const { jpush: config = {} } = require('config');
const logger = require('./logger');

const client = config.enable && JPush.buildClient(config);

const doPush = async (payload) => {
  const { title, content, receiver, extras = {} } = payload;

  const apnsProd = false;

  logger.info('start jpush with payload', payload);
  const extrasIos = {
    userId: extras.userId,
    target: (extras.target ? extras.target.toString() : '0'),
  };
  logger.info('ios extras', extrasIos);
  const pushPayload = client.push()
    .setPlatform(JPush.ALL)
    .setAudience(JPush.registration_id(receiver.join(',')))
    .setNotification(
      title,
      JPush.android(content, title, 1, extras),
      JPush.ios(content, 'sound', '+1', false, extrasIos),
    )
    .setOptions(null, 60, null, apnsProd);

  const push = Promise.promisifyAll(pushPayload);
  const result = await push.sendAsync();
  logger.info('result return by jpush is: ', result);

  if (!result || !result.msg_id) {
    logger.error('jpush failed with error');
    return null;
  }
  return result;
};

/**
 * @description  极光推送 
 * @param {object} [payload={}] - input arguments
 * @param {string} [payload.title] - 标题 
 * @param {string} [payload.content] - 内容 
 * @param {array} [payload.receiver] - 推送对象的regId数组
 * @param {object} [payload.extras] - 附加参数 
 * @return {object} 极光推送结果 
 */
exports.send = async payload =>
  // TODO validate payload
  doPush(payload)
    .catch(() => {
      logger.error('jpush failed with error', payload);
      return null;
    })
;
