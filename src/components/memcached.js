/*
* 组件基于 https://github.com/3rd-Eden/memcached 这个依赖包。
* 由于该module不支持promise，所以使用bluebird转化，所有方法需要加上Async。
* 如：get  => getAsync。 
* 具体方法请参考memcached包。
*/
const Memcached = require('memcached');
const Promise = require('bluebird');

const config = require('config').memcached;
const logger = require('./logger');

const memcached = new Memcached(config.hosts);

memcached.on('failure', (details) => {
  logger.error(`Server ${details.server} went down due to: ${details.messages.join('')}`);
});

memcached.on('reconnecting', (details) => {
  logger.debug(`Total downtime caused by server ${details.server} : ${details.totalDownTime} ms`);
});

module.exports = Promise.promisifyAll(memcached);
