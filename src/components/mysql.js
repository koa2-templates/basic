const Sequelize = require('sequelize');

const { mysql: config } = require('config');
const logger = require('./logger');

if (config.logging !== false) {
  config.logging = logger.info.bind(logger);
}

let options = {
  dialect: 'mysql',
  pool: {
    max: 35,
    min: 0,
    idle: 10000,
  },
  timezone: '+08:00',
  define: {
    // engine: 'MYISAM',
    underscored: true,
    timestamps: true,
    freezeTableName: true,
    createdAt: false,
    updatedAt: false,
    deletedAt: false,
  },
  // logging: config.logging
  logging(sql) {
    if (JSON.stringify(sql).indexOf('Executing')) {
      // console.log(sql);

    } else {
      logger.info(sql);
    }
  },
};
options = Object.assign(options, config.options);

module.exports = new Sequelize(
  config.name,
  config.username,
  config.password,
  options,
);
