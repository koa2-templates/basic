const _ = require('lodash');
const rp = require('request-promise');

const logger = require('../components/logger');

require('./request-debug')(rp, (type, data) => {
  switch (type) {
    case 'request': {
      logger.info(
        'remote-request',
        _.pick(data, ['debugId', 'uri', 'method', 'query']),
      );
      break;
    }
    case 'response': {
      logger.info(
        'remote-response',
        _.pick(data, ['debugId', 'statusCode', 'timingPhases']),
      );
      break;
    }
    default: {
      break;
    }
  }
});

module.exports = rp.defaults({
  json: true,
  time: true,
  timeout: 5 * 1000,
});
