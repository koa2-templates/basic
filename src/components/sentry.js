const Raven = require('raven');
const { sentry: config } = require('config');

if (config.enable && config.dns) {
  Raven.config(config.dsn, config.options).install();
}

module.exports = Raven;
