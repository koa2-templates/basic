const WechatApi = require('co-wechat-api');
const OAuth = require('co-wechat-oauth');
const config = require('config');

const cache = require('./memcached');
const logger = require('./logger');
const utils = require('../lib/utils');

const { appId, appSecret } = config.wechat.account;

const tokenKey = 'WECHAT_TOKEN_KEY';
const authTokenPrefix = 'WECHAT_AUTH_TOKEN#';

// exports.wechatApi = new WechatApi(appId, appSecret);
// exports.wechatOauth = new OAuth(appId, appSecret);

exports.wechatApi = new WechatApi(appId, appSecret, (async () => {
  // 传入一个获取全局token的方法
  const tokenInfo = await cache.getAsync(tokenKey);
  logger.info('wechat token in cache is', { tokenInfo });
  const token = tokenInfo ? JSON.parse(tokenInfo) : null;
  return token;
}), async (token) => {
  // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
  // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
  logger.info('save wechat api token', { token });
  const result = await cache.setAsync(tokenKey, JSON.stringify(token), 20 * 24 * 3600);
  logger.info('save wechat api token result', { result });
  return result;
});

exports.wechatOauth = new OAuth(appId, appSecret, (async (openid) => {
  // 传入一个获取全局token的方法
  const key = authTokenPrefix + openid;
  const tokenInfo = await cache.getAsync(key) || {};
  return JSON.parse(tokenInfo);
}), (async (openid, token) => {
    // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
    // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
    const key = authTokenPrefix + openid;
    const result = await cache.setAsync(key, JSON.stringify(token), 20 * 24 * 3600);
    return result;
  }));

/**
 * @description 发送微信消息模板 
 * @param {object} [payload={}] - input arguments
 * @param {string} [payload.openId] - 用户公众号id
 * @param {string} [payload.type] - 消息类型 
 * @param {string} [payload.data] - 数据 
 * @return {boolean} 是否成功
 */
exports.sendNotice = async (payload) => {
  const { openId, type, data, url } = payload;

  const templateCfg = config.wechat.template[type];
  if (!templateCfg) {
    return utils.throwErr('消息模板不存在');
  }

  const { id: templateId } = templateCfg;
  const redirectUrl = url || (config.wechat.host + templateCfg.url);
  let template = {};
  switch (type) {
    case 't2':
      template = {
        first: { value: '哈哈哈哈哈。' },
        keyword1: { value: `${data.name}`, color: '#8A2BE2' },
        keyword2: { value: `${data.age}`, color: '#8A2BE2' },
        keyword3: { value: `${data.gender}`, color: '#8A2BE2' },
        remark: {
          value: '哈哈哈哈哈',
        },
      };
      break;
    default:
      return utils.throwErr('消息模板不存在');
  }

  const result = await exports.wechatApi.sendTemplate(openId, templateId, redirectUrl, '', template);
  logger.info('the result of send template from wechat', { result });

  if (result && result.errcode === 0) {
    return result;
  }
  return utils.throwErr('发送模板消息失败');
};
