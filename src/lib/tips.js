module.exports = {
  NETWORK_WEAK: '网络异常，请稍后重试',

  USER_NOT_FOUND: '用户不存在',
  USER_SESSION_TIMEOUT: '会话超时，请重新登录',
  USER_REGISTER_SUCCESS: '用户注册成功',
  USER_OR_PASSWORD_ERROR: '用户名或密码错误',
  USER_LOGIN_SUCCESS: '用户登录成功',
  USER_LOGOUT_SUCCESS: '注销成功',
};
