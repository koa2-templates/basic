const _ = require('lodash');
const crypto = require('crypto');
const config = require('config');

/**
 * md5正常字符串加密
 */
function md5(str) {
  const ret = crypto.createHash('md5').update(str.toString()).digest('hex');
  return ret;
}

/**
 * md5中文字符串加密
 * @desc 解决中文加密不同的问题
 */
function md5Pay(str) {
  const s = (Buffer.from(str)).toString('binary');
  const ret = crypto.createHash('md5').update(s).digest('hex');
  return ret;
}

/**
 * aes 加密
 */
function encryptByAES(key, iv, data, type = 'aes-128-cbc') {
  // type = type || 'aes-128-cbc';  // aes-256-cbc
  const cipher = crypto.createCipheriv(type, key, iv);
  let crypted = cipher.update(data, 'utf8', 'binary');
  crypted += cipher.final('binary');
  crypted = Buffer.from(crypted, 'binary').toString('base64');
  return crypted;
}

/**
 * aes 解密
 */
function decryptByAES(key, iv, cryptedStr, type = 'aes-128-cbc') {
  // type = type || 'aes-128-cbc';
  const crypted = Buffer.from(cryptedStr, 'base64').toString('binary');
  const decipher = crypto.createDecipheriv(type, key, iv);
  let decoded = decipher.update(crypted, 'binary', 'utf8');
  decoded += decipher.final('utf8');
  return decoded;
}

/**
 * @desc 脱敏手机号 如：177****2580
 * @param {string} phone 
 */
function privacyPhone(phone) {
  return phone.replace(/(\d{3})(\d{4})(\d{4})/, '$1****$3');
}
/**
 * @desc 脱敏身份证
 * @param {string} certId 
 */
function privacyCertId(certId) {
  return certId.replace(/(\d{6})(\d{8})(\d{4}||(\d{3}[X]))/, '$1********$3');
}
/**
 * @desc 统一抛错方法
 * @param {string} msg 
 */
const throwErr = (msg) => {
  const err = new Error(msg);
  err.code = config.app.name;
  throw err;
};

const doReplace = (str, placement = []) => {
  if (!placement.length) return str;
  _.forEach(placement, (item) => {
    str = str.replace(item.pattern, item.value);
  });

  return str;
};

module.exports = {
  md5,
  md5Pay,
  encryptByAES,
  decryptByAES,
  throwErr,
  doReplace,
};
