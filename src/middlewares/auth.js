const moment = require('moment');
const models = require('../models');

module.exports = async (ctx, next) => {
  const token = ctx.request.get('token');

  if (!token) return ctx.unlogin();

  const session = await models.session.findOne({ where: { token } });

  if (!session || moment().isAfter(session.expire_date)) {
    return ctx.unlogin();
  }
  const user = await models.user.findById(session.user_id);
  ctx.user = user;

  await next();
};
