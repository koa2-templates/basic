/**
 * use async await to catch app error
 * @param {Object} [app] - koa app instance
 * @param {Object} [payload] - config for this middleware
 * @param {boolean} payload.sentry - whether enable sentry
 */

const config = require('config');

const sentry = require('../components/sentry');
const logger = require('../components/logger');

module.exports = () => async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    logger.error(err);
    ctx.status = 200;
    ctx.body = {
      status: 'error',
      content: null,
      msg: err.message,
    };
    if (err.code !== config.app.name
                && config.sentry.enable
                && process.env.NODE_ENV === 'test'
    ) {
      sentry.captureException(err, { request: ctx.req });
    }
  }
};
