const _ = require('lodash');

module.exports = (payload = {}) => {
  const { statusCode = 200 } = payload;

  return async (ctx, next) => {
    /**
        * desc: 正确response
        * @input content: 响应体
        * @input msg: 成功提示 
        */
    ctx.reply = (content, msg) => {
      ctx.status = statusCode;
      ctx.body = {
        status: 'success',
        msg,
        content,
      };
    };
    /**
         * @desc: warning response 
         * @param error 对应 lib/error_code 下定义的错误码 
         * @param content 额外需要返回的content
         */
    ctx.warn = (error, content = {}) => {
      ctx.status = statusCode;
      content = Object.assign({ code: error.code }, content);
      const result = _.isPlainObject(error) ?
        {
          status: 'warning',
          msg: error.msg,
          content,
        } : {
          status: 'warning',
          msg: error,
        };

      ctx.body = result;
    };

    /*
        * desc: unlogin response 
        */
    ctx.unlogin = () => {
      ctx.status = statusCode;
      ctx.body = {
        status: 'unlogin',
        content: null,
        msg: '会话超时，请重新登录',
      };
    };

    await next();
  };
};
