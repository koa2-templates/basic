const session = require('koa-session');

// const utils = require('../lib/utils');

const sessionConfig = {
  key: 'wid',
  maxAge: 20 * 24 * 3600,
  overwrite: true,
  httpOnly: true,
  signed: true,
  rolling: false,
  renew: false,
};


module.exports = app => session(sessionConfig, app);

