const config = require('config');
const router = require('koa-router')();
const validate = require('koa2-validation');

const user = require('./controllers/user');

module.exports = () => {
  // health check
  router.get('/check', ctx => (ctx.body = `${config.app.name} is working`));

  // for mongodb
  router.post('/users', validate(user.v.addUser), user.addUser);
  router.get('/users/:id', validate(user.v.getUserInfo), user.getUserInfo);
  router.get('/users', validate(user.v.getUserList), user.getUserList);

  return router.routes();
};
