const test = require('ava').test;
const helper = require('../helper');

test('check: should return hello message', async (t) => {
  const { statusCode, res } = await helper.request({
    url: '/check',
    qs: {},
  });

  const appName = helper.config.app.name;
  t.is(statusCode, 200);
  t.is(res.text, `${appName} is working`);
});

