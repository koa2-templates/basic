const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const Promise = require('bluebird');
const request = require('supertest');

const app = require('../app');

exports.config = require('config');

exports.loadDB = models => exports.dropDb().then(exports.initDb.bind(exports, models));

/**
 * initData
 * @param {Array|String} models - those models need to init data to db
 * @returns {Promise}
 */
exports.initDb = (models) => {
  const mods = _.isString(models) ? [models] : models;

  return Promise.map(mods, mod => module.require(`./${mod}/init`).initData());
};

exports.dropDb = () => {
  const tryDropDb = (file) => {
    try {
      return module.require(`./${file}/init`).drop();
    } catch (e) {
      return e;
    }
  };

  const readdir = Promise.promisify(fs.readdir, { context: fs });
  return readdir(path.join(__dirname, './')).map(tryDropDb);
};

exports.request = (options, query) => {
  const opts = _.isString(options) ? { url: options, query } : options;
  const method = opts.method || opts.methods || 'get';
  let req = request(app)[method](opts.url);
  if (opts.data) {
    req = req.send(opts.data);
  }

  if (opts.header && _.isObject(opts.header)) {
    _.each(opts.header, (val, key) => req.set(key, val));
  }

  return req
    .query(opts.query || opts.qs || {})
    .set('Accept', 'application/json');
};
